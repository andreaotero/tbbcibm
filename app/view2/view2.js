'use strict';

angular.module('myApp.view2', ['ngRoute'])

.config(['$routeProvider', function($routeProvider) {
  $routeProvider
  .when('/view2', {
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  })
  .when('/formulario',{
    templateUrl: 'view2/view2.html',
    controller: 'View2Ctrl'
  });
}])

.controller('View2Ctrl', ['$scope','$http',function($scope,$http) {
  $scope.master = {};

  $scope.update = function(user) {
    $scope.master = angular.copy(user);
  };

  $scope.reset = function(form) {
    if (form) {
      form.$setPristine();
      form.$setUntouched();
    }
    $scope.user = angular.copy($scope.master);
  };

  $scope.reset();

  $scope.pokemons = [];
  $scope.espacios = [];
  $scope.bicicletas = [];
  $scope.misas = [];
  $http({
    method: 'GET',
    url: 'https://pokeapi.co/api/v2/pokemon/ditto/'
 }).then(function (response){
  $scope.pokemons = response.data;

 },function (error){

 });
 $http({
  method: 'GET',
  url: 'http://www.asterank.com/api/mpc'
}).then(function (response){
$scope.espacios = response.data;

},function (error){

});

$http({
  method: 'GET',
  url: 'http://www.asterank.com/api/mpc'
}).then(function (response){
$scope.bicicletas = response.data;

},function (error){

});
$http({
  method: 'GET',
  url: 'http://www.asterank.com/api/mpc'
}).then(function (response){
$scope.misas = response.data;

},function (error){

});

}]);
